# CBun SDK

The CBun SDK is the Software Development Kit that provides all essential tools for CBun development. In order to develop CBuns with the CBun SDK you need a so called CBun build environment which is based on a certainly configured Ubuntu 18.04 Linux OS with a special set of software packages (such as Kassow Robots APIs).

## Build Environment Setup

The recommended CBun development environment setup is based on the **Visual Studio Code Dev Containers** extension. With this you can develop your CBun in Visual Studio Code on Windows, MacOS or Linux without the need for complicated build environment configuration. The VSCode Dev Containers extension allows you to open your CBun project folder in a preconfigured CBun development container that contains all you may need for the development, debugging and build of your CBun.   

### Prerequisities

To get started, follow these steps:

1. Install and configure [Docker](https://www.docker.com/get-started) for your operating system, using one of the paths below.

   **Windows / macOS:**

   1. Install Docker [Desktop for Windows/Mac](https://www.docker.com/products/docker-desktop).

   2. If you are using Windows, enable WSL and make sure it is up to date by running the following Command Prompt: `wsl --update`

   **Linux:**

   1. Follow the [official install instructions for Docker CE/EE for your distribution](https://docs.docker.com/install/#supported-platforms).

   2. Add your user to the docker group by using a terminal to run: `sudo usermod -aG docker $USER`

   3. Sign out and back in again so your changes take effect.

2. Install [Visual Studio Code](https://code.visualstudio.com/) or [Visual Studio Code Insiders](https://code.visualstudio.com/insiders/).

3. Install the [Dev Containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers). 

### CBun Project Setup

To configure your CBun project, follow these steps:

1. Clone this project to get access to the latest configuration files and CMakeLists.txt template.

        git clone git@gitlab.com:kassowrobots-sdk/cbun-sdk.git

2. Copy the `.devcontainer` and `.vscode` folders into your root CBun project folder

        cp -r ./cbun-sdk/.devcontainer YOUR_CBUN_FOLDER_PATH
        cp -r ./cbun-sdk/.vscode YOUR_CBUN_FOLDER_PATH

3. Copy the `CMakeLists.txt` file into the CBun backend subproject folder

        cp ./cbun-sdk/CMakeLists.txt YOUR_CBUN_FOLDER_PATH/backend/

3. Adjust the `CMakeLists.txt` to fit your CBun. 

4. Open your CBun project folder in Visual Studio Code.

5. Click **Yes, I trust the authors**.

5. Click **Reopen in Container** (this may take few minutes).

6. Select the **GCC 8.4.0 x86_64-linux-gnu** as the active Kit.

### CBun Tasks

Finally you can run the following tasks from the Visual Studio Code:

* **CBun: Build** - Builds and assembles your CBun from sources into the `build/<PROJECT_NAME>.cbun` file.

* **CBun: Build and Install** - Builds and installs your CBun (for integration tests only).



  
